package Repository;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Context{
    private MongoDatabase _database;
    private String _collection;
    private MongoClient _client;
    Context(String collection, MongoClient client)
    {
        _client = client;
        _collection = collection;
        _database = _client.getDatabase("laboratorMap");
    }
    MongoCollection getCollection()
    {
        return _database.getCollection(_collection);
    }
}
