package Repository;

import DataModel.Student;
import DataModel.Task;
import DataModel.Validator.Validator;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class StudentDatabase implements CrudRepository<ObjectId, Student> {

    private MongoCollection coll;
    private Validator<Student> _validator;
    public StudentDatabase(MongoClient client, String collection, Validator<Student> validator) {
        coll = new Context(collection, client).getCollection();
        _validator = validator;
    }
    @Override
    public Student findOne(ObjectId id) throws IllegalArgumentException {
        try {
            Document doc = (Document) coll.find(eq("_id", id)).first();
            return Student.getObj(doc);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    @Override
    public List<Student> findAll() {
        List<Student> result=new ArrayList<>();
        try (MongoCursor cursor = coll.find().iterator()) {
            while (cursor.hasNext()) {
                result.add(Student.getObj((Document) cursor.next()));
            }
        }
        return result;
    }

    @Override
    public ObjectId save(Student entity) {
        try{
            this._validator.validate(entity);
            Document doc =Document.parse(String.valueOf(entity));
            coll.insertOne(doc);
            return doc.getObjectId("_id");
        }
        catch (Exception ex){
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    @Override
    public boolean delete(ObjectId id) {
        DeleteResult deleteResult = coll.deleteOne(eq("_id", id));
        return deleteResult.getDeletedCount() == 1;
    }

    @Override
    public boolean update(Student entity) {
        try {
            this._validator.validate(entity);
            coll.replaceOne(eq("_id", entity.getID()), Document.parse(String.valueOf(entity)));
            return true;
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }
}