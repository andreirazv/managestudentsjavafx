package Controller.Login;

import Controller.Administrator.AdministratorView;
import Controller.LogInStudentView.LogInStudentView;
import DataModel.Account;
import DataModel.Student;
import Services.AccountService;
import Services.GradeService;
import Services.StudentService;
import Services.TaskService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {
    @FXML
    private TextField tfUsername;
    @FXML
    private TextField tfPassword;
    @FXML
    private Pane paneLoginForm;
    @FXML
    private StudentService studentService;
    private TaskService taskService;
    private GradeService gradeService;
    private AccountService accountService;
    private Stage loginStage;

    public void setServices(StudentService studentService,TaskService taskService,GradeService gradeService,AccountService accountService,Stage loginStage)
    {
        this.studentService=studentService;
        this.taskService=taskService;
        this.gradeService=gradeService;
        this.accountService=accountService;
        this.loginStage=loginStage;
    }
    @FXML
    private void btnLoginAction() {
        String username = tfUsername.getText();
        String password = tfPassword.getText();
        try {
            if(username.equals("")||password.equals(""))
            {showErrorMessage("Please insert username or password!");return;}
            Account account = accountService.getStudentByLogin(username, password);
                loginStage.close();
                if (account.getPermision().equals("Admin")) {
                    FXMLLoader fxmlLoader = new
                            FXMLLoader(getClass().getResource("/View/AdminView.fxml"));
                    Parent root = fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setTitle("Student Menu");
                    stage.setScene(new Scene(root));
                    AdministratorView controller = fxmlLoader.getController();
                     controller.setServices(studentService, accountService,account,this.loginStage,stage);
                    //controller.setStage(loginStage, stage);
                    // controller.setUser(account);
                    stage.show();
                }
                if(account.getPermision().equals("Student")||account.getPermision().equals("Teacher")){
                    FXMLLoader fxmlLoader = new
                            FXMLLoader(getClass().getResource("/View/mainMenuStudent.fxml"));
                    Parent root = fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setTitle("Student Menu");
                    stage.setScene(new Scene(root));
                    LogInStudentView controller = fxmlLoader.getController();
                    controller.setServices(studentService, taskService, gradeService, accountService);
                    controller.setStage(loginStage, stage);
                    controller.setUser(account);
                    stage.show();
                }
            }

        catch (Exception ex) {
            System.out.println(ex.getMessage());
            showErrorMessage(ex.getMessage());
            paneLoginForm.setDisable(false);
        }

    }
    @FXML
    private void btnCloseAction()
    {
        loginStage.close();
    }
    private static void showMessage(String text){
        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private static void showErrorMessage(String text){
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }
}
