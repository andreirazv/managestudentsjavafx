package Controller.LogInStudentView;

import Controller.GradeView.GradeViewStudent;
import Controller.GradeView.GradeViewTeacher;
import Controller.StudentView.StudentGroupView;
import Controller.StudentView.StudentViewTeacher;
import Controller.TaskView.TaskViewStudent;
import Controller.TaskView.TaskViewTeacher;
import DataModel.Account;
import DataModel.Student;
import Services.AccountService;
import Services.GradeService;
import Services.StudentService;
import Services.TaskService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.bson.types.ObjectId;

import java.io.IOException;

public class LogInStudentView {
    @FXML
    private Label labelName;
    @FXML
    private Label labelPrenume;
    @FXML
    private Label labelRole;
    @FXML
    private Label labelGroup;
    @FXML
    private Label labelEmail;
    @FXML
    private Label labelFNameInfo;
    @FXML
    private Pane paneInfoStudent;
    @FXML
    private Label labelExtraPoints;
    private Account account;
    private Student student=null;
    private StudentService studentService;
    private TaskService taskService;
    private GradeService gradeService;
    private AccountService accountService;
    private Stage loginStage;
    private Stage loginStudentStage;
    public void setUser(Account account)
    {
        this.account=account;
        if(!account.getStudentId().equals("null"))
        {
            labelFNameInfo.setText("Prenume:");
            paneInfoStudent.setVisible(true);
            this.student = studentService.getStudent(new ObjectId(account.getStudentId()));
            if(student==null)
                throw new IllegalArgumentException("Acest student nu exista in baza de date!");
        }
        else
        {
            labelFNameInfo.setText("Permisiuni:");
            labelPrenume.setText(account.getPermision());
            labelName.setText(account.getName());
            paneInfoStudent.setVisible(false);
        }
        init();
    }
    public void setStage(Stage loginStage,Stage loginStudentStage)
    {
        this.loginStudentStage=loginStudentStage;
        this.loginStage=loginStage;
    }
    public void setServices(StudentService studentService,TaskService taskService,GradeService gradeService,AccountService accountService)
    {
        this.gradeService=gradeService;
        this.taskService=taskService;
        this.studentService=studentService;
        this.accountService=accountService;

    }
    private void init(){
        initInfoStudent();
    }
    private void initInfoStudent()
    {
        if(student!=null)
        {
            labelName.setText(student.getName());
            labelPrenume.setText(student.getFirstName());
            labelRole.setText(account.getPermision());
            labelGroup.setText(student.getGroup()+"");
            labelEmail.setText(student.getEmail());
            labelExtraPoints.setText(student.getExtraPoints()+"");
        }
    }

    @FXML
    private void btnLogoutAction(){
        this.account=null;
        this.loginStudentStage.close();
        this.loginStage.show();
    }
    @FXML
    private void btnStudentAction(){

        try {
            if(account.getPermision().equals("Student"))
            {
                loginStudentStage.close();
                FXMLLoader fxmlLoader = new
                        FXMLLoader(getClass().getResource("/View/studentGroupView.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Student View(Only Students)");
                stage.setScene(new Scene(root));
                StudentGroupView controller = fxmlLoader.getController();
                controller.setStudent(this.student,studentService,loginStudentStage,stage);
                stage.show();
            }
            if(account.getPermision().equals("Teacher"))
            {
                loginStudentStage.close();
                FXMLLoader fxmlLoader = new
                        FXMLLoader(getClass().getResource("/View/StudentViewTeacher.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Student View(Only Teachers)");
                stage.setScene(new Scene(root));
                StudentViewTeacher controller = fxmlLoader.getController();
                controller.setStudent(this.account,studentService,loginStudentStage,stage);
                stage.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @FXML
    private void btnTaskAction(){
        try {
            if (account.getPermision().equals("Student")) {
                loginStudentStage.close();
                FXMLLoader fxmlLoader = new
                        FXMLLoader(getClass().getResource("/View/TaskViewStudent.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Task View(Only Students)");
                stage.setScene(new Scene(root));
                TaskViewStudent controller = fxmlLoader.getController();
                controller.setStudent(this.student, studentService, taskService,loginStudentStage, stage);
                stage.show();
            }
            if (account.getPermision().equals("Teacher")) {
                loginStudentStage.close();
                FXMLLoader fxmlLoader = new
                        FXMLLoader(getClass().getResource("/View/TaskViewTeacher.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Task View(Only Teacher)");
                stage.setScene(new Scene(root));
                TaskViewTeacher controller = fxmlLoader.getController();
                controller.setInfo(account, studentService, taskService,loginStudentStage, stage);
                stage.show();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    private void btnCatalogAction() {
        try {
            if (account.getPermision().equals("Student")) {
                loginStudentStage.close();
                FXMLLoader fxmlLoader = new
                        FXMLLoader(getClass().getResource("/View/GradeViewStudent.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Grade View(Only Students)");
                stage.setScene(new Scene(root));
                GradeViewStudent controller = fxmlLoader.getController();
                controller.setStudent(this.student, studentService, taskService,gradeService,loginStudentStage, stage);
                stage.show();
            }
            if (account.getPermision().equals("Teacher")) {
                loginStudentStage.close();
                FXMLLoader fxmlLoader = new
                        FXMLLoader(getClass().getResource("/View/GradeViewTeacher.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Grade View(Only Teacher)");
                stage.setScene(new Scene(root));
                GradeViewTeacher controller = fxmlLoader.getController();
                controller.setInfo(this.account, studentService, taskService,gradeService,loginStudentStage, stage);
                stage.show();
                }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
