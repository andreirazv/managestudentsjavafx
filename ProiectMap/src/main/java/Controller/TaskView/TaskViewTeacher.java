package Controller.TaskView;

import Controller.Observer.AutoCompleteComboBoxListener;
import DataModel.Account;
import DataModel.Student;
import DataModel.Task;
import Services.StudentService;
import Services.TaskService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class TaskViewTeacher {
    @FXML
    private Label labelTeacherName;
    @FXML
    private TableColumn<Task, String> collDescription;
    @FXML
    private TableColumn<Task,String> collTeacher;
    @FXML
    private TableColumn<Task, String> collStudent;
    @FXML
    private TableColumn<Task, LocalDate> collDeadLine;
    @FXML
    private ComboBox<String> cboxStudentFiltr;
    @FXML
    private ComboBox<String> cboxGroupFiltr;
    @FXML
    private ComboBox<String> cboxStudent;
    @FXML
    private ComboBox<String> cboxGroup;
    @FXML
    private TextField tfTeacher;
    @FXML
    private DatePicker dPickerDateLine;
    @FXML
    private TextArea areaDescription;
    @FXML
    private TableView<Task> tableView;

    private Stage prevStage;
    private Stage thisStage;
    private StudentService studentService;
    private TaskService taskService;
    private Account account;
    private ObservableList<Task> model = FXCollections.observableArrayList();

    public void setInfo(Account account, StudentService studentService, TaskService taskService, Stage prevStage, Stage thisStage) {
        this.studentService = studentService;
        this.taskService = taskService;
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.account = account;
        init();
        listen();

    }
    private void listen()
    {
        tableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetails(newValue));

        collStudent.setCellFactory(column -> {
            TableCell <Task, String> cell = new TableCell <Task, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Student st=studentService.getStudent(new ObjectId(item));
                        setText(st.getName()+" "+st.getFirstName()); } }};return cell; });
    }
    private void selectTask(){
            model.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw().spliterator(), false)
                    .collect(Collectors.toList()));
            tableView.setItems(model);
    }
    private void showDetails(Task taskRecv)
    {
        if(taskRecv!=null)
        {
            Student st = studentService.getStudent(new ObjectId(taskRecv.getStudentId()));
            areaDescription.setText(taskRecv.getDescription());
            tfTeacher.setText(taskRecv.getTeacher());
            dPickerDateLine.setValue(taskRecv.getDeadLine());
            cboxStudent.setValue(st.getName()+" "+st.getFirstName());
            cboxGroup.setValue(st.getGroup()+"");
        }
    }
    public void btnAddTaskAction()
    {
        try {
            Student st = studentService.getStudentByName(cboxStudent.getValue());
            this.taskService.saveTask(new ObjectId(),areaDescription.getText(),dPickerDateLine.getValue(),st.getID(),account.getName(),account);
            showMessage("Task Added!");
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
   }
    public void btnUpdateTaskAction()
    {
        try {
            if(tableView.getSelectionModel().isEmpty())
            { showErrorMessage("Please select task from table!");return;}
            Task task = tableView.getSelectionModel().getSelectedItem();
            if(task!=null)
            {
                taskService.updateTaskDeadLine(task.getID(),1,account.getName(),account);
                showMessage("Task Updated!");
            }
            else
                showErrorMessage("Something is wrong!");
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnDeleteTaskAction()
    {
        try {
            if(tableView.getSelectionModel().isEmpty())
            { showErrorMessage("Please select task from table!");return;}
            Task task = tableView.getSelectionModel().getSelectedItem();
            if(task!=null) {
                taskService.deleteTask(task.getID());
                showMessage("Task Deleted!");
            }
            else showErrorMessage("Something is wrong!");
        }
        catch (Exception ex) {
            showErrorMessage(ex.getMessage());
        }
    }
    private void initComboBox()
    {
        initComboBoxStudent();
        initComboBoxGroup();
    }
    private void initComboBoxStudent()
    {
        List<String> list=StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .map(st ->st.getName()+" "+st.getFirstName())
                .collect(Collectors.toList());
        List<String> listWithoutDuplicates = new ArrayList<>(
                new HashSet<>(list));
        cboxStudent.getItems().addAll(listWithoutDuplicates);
        cboxStudentFiltr.getItems().addAll(listWithoutDuplicates);

        new AutoCompleteComboBoxListener<String>(cboxStudent);
        new AutoCompleteComboBoxListener<String>(cboxStudentFiltr);
    }
    private void initComboBoxGroup()
    {
        List<String> list=StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .map(st ->st.getGroup()+"")
                .collect(Collectors.toList());
        List<String> listWD = new ArrayList<>(
                new HashSet<>(list));
        cboxGroup.getItems().addAll(listWD);
        cboxGroupFiltr.getItems().addAll(listWD);

        new AutoCompleteComboBoxListener<String>(cboxGroup);
        new AutoCompleteComboBoxListener<String>(cboxGroupFiltr);
    }
    private void init()
    {
        initComboBox();
        this.labelTeacherName.setText(account.getName());
        selectTask();
        collDeadLine.setCellValueFactory(new PropertyValueFactory<>("deadLine"));
        collTeacher.setCellValueFactory(new PropertyValueFactory <>("teacher"));
        collDescription.setCellValueFactory(new PropertyValueFactory <>("description"));
        collStudent.setCellValueFactory(new PropertyValueFactory <>("studentId"));
        tableView.setItems(model);
        dPickerDateLine.setValue(LocalDate.now());
    }
    public void btnBackAction()
    {
        thisStage.close();
        prevStage.show();
    }
    public void cboxGroupFiltrAction()
    {
        if(cboxGroupFiltr.getValue()!=null&&!cboxGroupFiltr.getValue().equals("")) {
            model.clear();
            model.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(Integer.parseInt(cboxGroupFiltr.getValue())).spliterator(), false)
                    .collect(Collectors.toList()));
            tableView.setItems(model);
        }
    }
    public void cboxStudentFiltrAction() {
        if (cboxStudentFiltr.getValue()!=null&&!cboxStudentFiltr.getValue().equals("")) {
            model.clear();
            model.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(cboxStudentFiltr.getValue()).spliterator(), false)
                    .collect(Collectors.toList()));
            tableView.setItems(model);
        }
    }
    public void cboxStudentAction()
    {
        if (cboxStudent.getValue()!=null&&!cboxStudent.getValue().equals("")) {
            int group = studentService.getGroupByName(cboxStudent.getValue());
            cboxGroup.setValue(group+"");
        }
    }
    private void showMessage(String text){
        selectTask();
        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private void showErrorMessage(String text){
        selectTask();
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }
}
