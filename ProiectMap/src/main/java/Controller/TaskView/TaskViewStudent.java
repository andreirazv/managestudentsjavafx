package Controller.TaskView;

import DataModel.Student;
import DataModel.Task;
import Services.StudentService;
import Services.TaskService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class TaskViewStudent {
    @FXML
    private Label labelStudent;
    private Student student;
    @FXML
    private TableColumn<Task, String> collDescription;
    @FXML
    private TableColumn<Task,String> collTeacher;
    @FXML
    private TableColumn<Task, String> collDeadline;
    @FXML
    private CheckBox checkBox;

    @FXML
    private TableView<Task> tableView;
    private Stage prevStage;
    private Stage thisStage;
    private StudentService studentService;
    private TaskService taskService;
    private ObservableList<Task> model = FXCollections.observableArrayList();

    public void setStudent(Student student, StudentService studentService, TaskService taskService, Stage prevStage, Stage thisStage) {
        this.studentService = studentService;
        this.taskService = taskService;
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.student = student;
        init();
        listen();

    }
    private void listen()
    {
        checkBox.selectedProperty().addListener((observable) -> selectTask());
    }
    private void selectTask(){
        if(checkBox.isSelected())
        {
            model.setAll(StreamSupport.stream(taskService.findAllFinishedHw(student).spliterator(), false)
                    .collect(Collectors.toList()));
        }
        else
        {
            model.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(student).spliterator(), false)
                    .collect(Collectors.toList()));
        }
    }
    private void init()
    {
        this.labelStudent.setText(student.getName()+" "+student.getFirstName());
        selectTask();
        collDeadline.setCellValueFactory(new PropertyValueFactory<>("deadLine"));
        collTeacher.setCellValueFactory(new PropertyValueFactory <>("teacher"));
        collDescription.setCellValueFactory(new PropertyValueFactory <>("description"));
        tableView.setItems(model);
    }
    public void btnBackAction()
    {
        thisStage.close();
        prevStage.show();
    }
}
