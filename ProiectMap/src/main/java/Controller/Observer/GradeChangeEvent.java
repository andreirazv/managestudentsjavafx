package Controller.Observer;

import DataModel.Grade;

public class GradeChangeEvent implements Event {
    private ChangeEventType type;
    private Grade grade, oldData;

    public GradeChangeEvent(ChangeEventType type, Grade data) {
        this.type = type;
        this.grade = data;
    }

    public ChangeEventType getType() {
        return type;
    }

    public Grade getData() {
        return grade;
    }

    public Grade getOldData() {
        return oldData;
    }
}