package Controller.Observer;

import DataModel.Task;

public class TaskChangeEvent implements Event {
    private ChangeEventType type;
    private Task data, oldData;

    TaskChangeEvent(ChangeEventType type, Task data) {
        this.type = type;
        this.data = data;
    }

    public ChangeEventType getType() {
        return type;
    }

    public Task getData() {
        return data;
    }

    public Task getOldData() {
        return oldData;
    }
}