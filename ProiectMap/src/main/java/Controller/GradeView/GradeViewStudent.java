package Controller.GradeView;

import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import Services.GradeService;
import Services.StudentService;
import Services.TaskService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GradeViewStudent {
    @FXML
    private Label labelStudent;
    private Student student;
    @FXML
    private TableColumn<Grade, String> collGrade;
    @FXML
    private TableColumn<Grade, String> collDate;
    @FXML
    private TableColumn<Grade,String> collTeacher;
    @FXML
    private TableColumn<Grade, String> collFeedback;

    @FXML
    private TableView<Grade> tableView;
    private Stage prevStage;
    private Stage thisStage;
    private StudentService studentService;
    private TaskService taskService;
    private GradeService gradeService;
    private ObservableList<Grade> model = FXCollections.observableArrayList();

    public void setStudent(Student student, StudentService studentService, TaskService taskService, GradeService gradeService, Stage prevStage, Stage thisStage) {
        this.studentService = studentService;
        this.taskService = taskService;
        this.gradeService = gradeService;
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.student = student;
        init();
    }
    private void init()
    {
        model.setAll(StreamSupport.stream(gradeService.findAllGrades(student).spliterator(), false)
                .collect(Collectors.toList()));
        this.labelStudent.setText(student.getName()+" "+student.getFirstName());
        collDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        collTeacher.setCellValueFactory(new PropertyValueFactory <>("teacher"));
        collFeedback.setCellValueFactory(new PropertyValueFactory <>("feedback"));
        collGrade.setCellValueFactory(new PropertyValueFactory <>("grade"));
        tableView.setItems(model);
    }
    public void btnBackAction()
    {
        thisStage.close();
        prevStage.show();
    }
}
