package Controller.GradeView;

import Controller.Observer.AutoCompleteComboBoxListener;
import DataModel.Account;
import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import Services.GradeService;
import Services.StudentService;
import Services.TaskService;
import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.bson.types.ObjectId;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GradeViewTeacher {

    public CheckBox checkBoxMotivated;
    public TextArea areaFeedback;
    public TextField tfGrade;
    public ComboBox<String> cboxTeacher;
    public ComboBox<String> cboxGroupFiltr;
    public ComboBox<String> cboxGroup;
    public ComboBox<String> cboxStudentFiltr;
    public ComboBox<String> cboxStudent;
    public Label labelTeacherName;
    public TabPane tabPane;
    public Tab tabTask;
    public Tab tabGrade;
    public TableView<Grade> tableViewGrade;
    public TableView<Task> tableViewTask;

    public TableColumn<Task, String> collStudentTask;
    public TableColumn<Task, String> collDescriptionTask;
    public TableColumn<Task, LocalDate> collDeadLineTask;
    public TableColumn<Task, String> collTeacherTask;

    public TableColumn<Grade, String> collStudentGrade;
    public TableColumn<Grade, Double> collGrade;
    public TableColumn<Grade, String> collTeacherGrade;
    public TableColumn<Grade, String> collFeedbackGrade;
    public TableColumn<Grade, LocalDate> collDateGrade;

    private Stage prevStage;
    private Stage thisStage;
    private Account account;
    private StudentService studentService;
    private GradeService gradeService;
    private TaskService taskService;
    private ObservableList<Task> modelTask = FXCollections.observableArrayList();
    private ObservableList<Grade> modelGrade = FXCollections.observableArrayList();
    private boolean isGrade;
    public DatePicker filterDateFrom;
    public DatePicker filterDateTo;
    public ComboBox<String> cboxTaskFiltr;

    public void setInfo(Account account, StudentService studentService, TaskService taskService,GradeService gradeService, Stage prevStage, Stage thisStage) {
        this.studentService = studentService;
        this.taskService = taskService;
        this.gradeService = gradeService;
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.account = account;
        isGrade=true;
        todayBtn=false;
        init();
        listen();
    }

    private void init()
    {
        initComboBox();

        this.labelTeacherName.setText(account.getName());
        collDeadLineTask.setCellValueFactory(new PropertyValueFactory<>("deadLine"));
        collDescriptionTask.setCellValueFactory(new PropertyValueFactory <>("description"));
        collStudentTask.setCellValueFactory(new PropertyValueFactory <>("studentId"));
        collTeacherTask.setCellValueFactory(new PropertyValueFactory <>("teacher"));

        collDateGrade.setCellValueFactory(new PropertyValueFactory<>("date"));
        collGrade.setCellValueFactory(new PropertyValueFactory <>("grade"));
        collFeedbackGrade.setCellValueFactory(new PropertyValueFactory <>("feedback"));
        collStudentGrade.setCellValueFactory(new PropertyValueFactory <>("taskId"));
        collTeacherGrade.setCellValueFactory(new PropertyValueFactory <>("teacher"));
        update();
    }
    private void listen()
    {
        tableViewGrade.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> selectGradeInfo(newValue));

        tableViewTask.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> selectTaskInfo(newValue));

        collStudentTask.setCellFactory(column -> {
            TableCell <Task, String> cell = new TableCell <Task, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Student st=studentService.getStudent(new ObjectId(item));
                        setText(st.getName()+" "+st.getFirstName()); } }};return cell; });
        collStudentGrade.setCellFactory(column -> {
            TableCell <Grade, String> cell = new TableCell <Grade, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Task tk = taskService.getTask(new ObjectId(item));
                        Student st=studentService.getStudent(new ObjectId(tk.getStudentId()));
                        setText(st.getName()+" "+st.getFirstName()); } }};return cell; });
    }
    public void btnResetAllAction()
    {
        filterDateFrom.setValue(LocalDate.now());
        filterDateTo.setValue(LocalDate.now());
        cboxStudentFiltr.getSelectionModel().clearSelection();
        cboxGroupFiltr.getSelectionModel().clearSelection();
        clearInfo();
        update();
    }
    private void selectGradeInfo(Grade grade) {
        if (grade != null) {
            clearInfo();
            Task task = taskService.getTask(new ObjectId(grade.getTaskId()));
            Student student = studentService.getStudent(new ObjectId(task.getStudentId()));
            tfGrade.setText(grade.getGrade() + "");
            cboxTeacher.setValue(grade.getTeacher());
            cboxStudent.setValue(student.getName() + " " + student.getFirstName());
            cboxGroup.setValue(student.getGroup() + "");
            areaFeedback.setText(grade.getFeedback());
        }
    }

    public void btnViewByGroupAndTask()
    {
        if(isGrade) {
            if (!cboxTaskFiltr.getSelectionModel().isEmpty()&&!cboxGroupFiltr.getSelectionModel().isEmpty()) {
                String taskStr[]=cboxTaskFiltr.getSelectionModel().getSelectedItem().split(" ");
                int nrWeek = Integer.parseInt(taskStr[1]);

                modelGrade.setAll(StreamSupport.stream(gradeService.findAllGradesByDate(nrWeek,cboxGroupFiltr.getSelectionModel().getSelectedItem()).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewGrade.setItems(modelGrade);
            } else
                update();
        }
        else
            showMessage("Please select Grade Tab and try again");
    }
    public void cboxTaskFiltrAction() {
        if((cboxTaskFiltr.getValue()!=null)&&(!cboxTaskFiltr.getValue().equals("")))
        try {
            if (isGrade) {
                if (!cboxTaskFiltr.getSelectionModel().isEmpty()) {
                    String taskStr[] = cboxTaskFiltr.getSelectionModel().getSelectedItem().split(" ");
                    int nrWeek = Integer.parseInt(taskStr[1]);
                    modelGrade.setAll(StreamSupport.stream(gradeService.findAllGradesByDate(nrWeek).spliterator(), false)
                            .collect(Collectors.toList()));
                    tableViewGrade.setItems(modelGrade);
                } else
                    update();
            } else
            {if(!cboxTaskFiltr.getSelectionModel().getSelectedItem().equals(""));
                   if(!isGrade) showMessage("Please select Grade Tab and try again!");}
        } catch (Exception ex) {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnFilterByDate()
    {
        LocalDate from = filterDateFrom.getValue();
        LocalDate to = filterDateTo.getValue();
        if(!isGrade)
        {
            modelTask.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(from,to).spliterator(), false)
                    .collect(Collectors.toList()));
            tableViewTask.setItems(modelTask);
        } else {
            modelGrade.setAll(StreamSupport.stream(gradeService.findAllGrades(from,to).spliterator(), false)
                    .collect(Collectors.toList()));
            tableViewGrade.setItems(modelGrade);
        }
    }

    private void clearInfo()
    {
        if(tfGrade!=null)
            tfGrade.clear();
        if(cboxTeacher!=null)
            cboxTeacher.getSelectionModel().clearSelection();
        if(cboxStudent!=null)
            cboxStudent.getSelectionModel().clearSelection();
        if(cboxGroup!=null)
            cboxGroup.getSelectionModel().clearSelection();
        if(cboxTaskFiltr!=null)
            cboxTaskFiltr.getSelectionModel().clearSelection();
        if(areaFeedback!=null)
            areaFeedback.clear();
        if(checkBoxMotivated!=null)
            checkBoxMotivated.setSelected(false);

    }
    public void btnExportPdfAction()
    {
        if(isGrade)
        {
            Iterable<Grade> grades=tableViewGrade.getItems();
            new Thread(() -> openFile(this.gradeToPdf(grades))).start();
        }
        else
        {
            Iterable<Task> tasks =tableViewTask.getItems();
            new Thread(() -> openFile(this.taskToPdf(tasks))).start();
        }
        showMessage("Your file will be opened after it has been created!");

    }
    private void selectTaskInfo(Task task)
    {
        if(task!=null)
        {
            clearInfo();
            Student student = studentService.getStudent(new ObjectId(task.getStudentId()));
            cboxStudent.setValue(student.getName()+" "+student.getFirstName());
        }
    }
    public void tabGradeAction() {
        if (!isGrade) {
            isGrade = true;
            clearInfo();
            if(tableViewGrade!=null)
                tableViewGrade.getSelectionModel().clearSelection();
            if(tableViewTask!=null)
                tableViewTask.getSelectionModel().clearSelection();
        }
    }
    public void tabTaskAction()
    {
        if(isGrade) {
            isGrade = false;
            clearInfo();
            if(tableViewGrade!=null)
                tableViewGrade.getSelectionModel().clearSelection();
            if(tableViewTask!=null)
                tableViewTask.getSelectionModel().clearSelection();
        }
    }
    public void cboxGroupFiltrAction() {
        cboxStudentFiltr.getSelectionModel().clearSelection();

        if ( cboxStudentFiltr.getSelectionModel().isEmpty()&&cboxGroupFiltr.getSelectionModel().isEmpty())
            update();
        if (!cboxGroupFiltr.getSelectionModel().isEmpty()) {
            if (!isGrade) {
                modelTask.clear();
                modelTask.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(Integer.parseInt(cboxGroupFiltr.getValue())).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewTask.setItems(modelTask);
            } else {
                modelGrade.clear();
                modelGrade.setAll(StreamSupport.stream(gradeService.findAllGrades(Integer.parseInt(cboxGroupFiltr.getValue())).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewGrade.setItems(modelGrade);
            }
        }

    }
    public void cboxStudentFiltrAction() {
        cboxGroupFiltr.getSelectionModel().clearSelection();

        if ( cboxStudentFiltr.getSelectionModel().isEmpty()&&cboxGroupFiltr.getSelectionModel().isEmpty())
            update();
        if (!cboxStudentFiltr.getSelectionModel().isEmpty()) {
            if (!isGrade) {
                modelTask.clear();
                modelTask.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(cboxStudentFiltr.getValue()).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewTask.setItems(modelTask);
            } else {
                modelGrade.clear();
                modelGrade.setAll(StreamSupport.stream(gradeService.findAllGrades(cboxStudentFiltr.getValue()).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewGrade.setItems(modelGrade);

            }
        }
    }
    public void cboxStudentAction()
    {
        if (cboxStudent.getValue()!=null&&!cboxStudent.getValue().equals("")) {
            int group = studentService.getGroupByName(cboxStudent.getValue());
            cboxGroup.setValue(group+"");
        }
    }
    public void cboxGroupAction()
    {
        if (cboxGroup.getValue()!=null&&!cboxGroup.getValue().equals("")) {
            List<String> grList = new ArrayList <>();
            studentService.findAllStudent(cboxGroup.getValue(),false).forEach(n->grList.add(n.getName()+" "+n.getFirstName()));
            List<String> listWithoutDuplicates = new ArrayList<>(
                    new HashSet<>(grList));
            cboxStudent.getItems().addAll(listWithoutDuplicates);
        }
        else
        {
            initComboBoxStudent();
        }
    }
    public void btnBackAction()
    {
        this.thisStage.close();
        this.prevStage.show();
    }
    private boolean todayBtn;
    public void btnTodayAction()
    {
        if(!todayBtn) {
            if (!isGrade) {
                modelTask.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw(LocalDate.now()).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewTask.setItems(modelTask);
            } else {
                modelGrade.setAll(StreamSupport.stream(gradeService.findAllGrades(LocalDate.now()).spliterator(), false)
                        .collect(Collectors.toList()));
                tableViewGrade.setItems(modelGrade);
            }
        }
        else
        {
            update();
        }
        todayBtn=!todayBtn;
    }
    private void update()
    {
        modelGrade.clear();
        modelTask.clear();
        modelTask.setAll(StreamSupport.stream(taskService.findAllNotFinishedHw().spliterator(), false)
                .collect(Collectors.toList()));
        tableViewTask.setItems(modelTask);
        modelGrade.setAll(StreamSupport.stream(gradeService.findAllGrades().spliterator(), false)
                .collect(Collectors.toList()));
        tableViewGrade.setItems(modelGrade);
    }
    public void btnAddGradeAction()
    {
        try {
            Task task = tableViewTask.getSelectionModel().getSelectedItem();

            if ((!tableViewGrade.getSelectionModel().isEmpty()) || tableViewTask.getSelectionModel().isEmpty()||task == null)
            {
                showMessage("Please select a task from task tab");

                return;
            }
            gradeService.saveGrade(new ObjectId(), Double.parseDouble(tfGrade.getText()), account.getName(), LocalDate.now(), task.getID(), areaFeedback.getText(), checkBoxMotivated.isSelected(),account);
            showMessage("Grade added");update();
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }

    private void showMessage(String text){

        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private void showErrorMessage(String text){
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }
    public void btnDeleteGradeAction()
    {
        try {

            Grade grade = tableViewGrade.getSelectionModel().getSelectedItem();
            if (tableViewGrade.getSelectionModel().isEmpty()||grade == null)
            {showMessage("Please select a grade from grade tab");return;}
            gradeService.deleteGrade(grade.getID());
            showMessage("Grade deleted");update();
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnUpdateGradeAction()
    {
        try {
            Grade grade = tableViewGrade.getSelectionModel().getSelectedItem();
            if (tableViewGrade.getSelectionModel().isEmpty()||grade == null)
            { showMessage("Please select a grade from grade tab");return;}
            gradeService.updateGrade(grade.getID(), Double.parseDouble(tfGrade.getText()), account.getName(), LocalDate.now(), new ObjectId(grade.getTaskId()), areaFeedback.getText());
            showMessage("Grade updated");update();
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    private void initComboBox()
    {
        initComboBoxStudent();
        initComboBoxGroup();
        initComboBoxTask();
    }
    private void initComboBoxStudent()
    {
        List<String> list= StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .map(st ->st.getName()+" "+st.getFirstName())
                .collect(Collectors.toList());
        List<String> listWithoutDuplicates = new ArrayList<>(
                new HashSet<>(list));
        cboxStudent.getItems().addAll(listWithoutDuplicates);
        cboxStudentFiltr.getItems().addAll(listWithoutDuplicates);

        new AutoCompleteComboBoxListener<String>(cboxStudent);
        new AutoCompleteComboBoxListener<String>(cboxStudentFiltr);
    }
    private void initComboBoxTask()
    {
        List<String> list=new ArrayList<>();
        for(int i =1;i<=14;i++)
            list.add("Tema "+i);
        cboxTaskFiltr.getItems().addAll(list);

        new AutoCompleteComboBoxListener<String>(cboxTaskFiltr);
    }
    private void initComboBoxGroup()
    {
        List<String> list=StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .map(st ->st.getGroup()+"")
                .collect(Collectors.toList());
        List<String> listWD = new ArrayList<>(
                new HashSet<>(list));
        cboxGroup.getItems().addAll(listWD);
        cboxGroupFiltr.getItems().addAll(listWD);

        new AutoCompleteComboBoxListener<String>(cboxGroup);
        new AutoCompleteComboBoxListener<String>(cboxGroupFiltr);
    }
    private String taskToPdf(Iterable<Task> lst)
    {
        System.out.println("Start writing pdf");
        /*try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }*/
        Document document = new Document();
        try
        {
            Random rnd = new Random();
            int nr = (rnd.nextInt(999999-9999) + 9999);
            String fileName = "Raport"+nr+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
            document.open();

            PdfPTable table = new PdfPTable(4);
            table.setWidthPercentage(100);
            table.setSpacingBefore(10f);
            table.setSpacingAfter(10f);

            float[] columnWidths = {1f, 1f, 1f,1f};
            table.setWidths(columnWidths);
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

            PdfPCell cell1 = new PdfPCell(new Paragraph("Student Name",boldFont));
            cell1.setBorderColor(BaseColor.BLACK);
            cell1.setPaddingLeft(10);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell1.setBorderWidthTop(2);
            PdfPCell cell2 = new PdfPCell(new Paragraph("Description",boldFont));
            cell2.setBorderColor(BaseColor.BLACK);
            cell2.setPaddingLeft(10);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell2.setBorderWidthTop(2);
            PdfPCell cell3 = new PdfPCell(new Paragraph("Teacher",boldFont));
            cell3.setBorderColor(BaseColor.BLACK);
            cell3.setPaddingLeft(10);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell3.setBorderWidthTop(2);
            PdfPCell cell4 = new PdfPCell(new Paragraph("Dead Line",boldFont));
            cell4.setBorderColor(BaseColor.BLACK);
            cell4.setPaddingLeft(10);
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell4.setBorderWidthTop(2);
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);

            lst.forEach(x->{
                Student student = studentService.getStudent(new ObjectId(x.getStudentId()));
                PdfPCell cell11 = new PdfPCell(new Paragraph(student.getName()+" "+student.getFirstName()));
                cell11.setBorderColor(BaseColor.BLACK);
                cell11.setPaddingLeft(10);
                cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell21 = new PdfPCell(new Paragraph(x.getDescription()));
                cell21.setBorderColor(BaseColor.BLACK);
                cell21.setPaddingLeft(10);
                cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell31 = new PdfPCell(new Paragraph(x.getTeacher()));
                cell31.setBorderColor(BaseColor.BLACK);
                cell31.setPaddingLeft(10);
                cell31.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell31.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell41 = new PdfPCell(new Paragraph(x.getDeadLine().toString()));
                if(x.getDeadLine().isAfter(LocalDate.now()))
                    cell41.setBorderColor(BaseColor.GREEN);
                else
                    cell41.setBorderColor(BaseColor.RED);
                cell41.setPaddingLeft(10);
                cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell41.setVerticalAlignment(Element.ALIGN_MIDDLE);
                table.addCell(cell11);
                table.addCell(cell21);
                table.addCell(cell31);
                table.addCell(cell41);

            });
            document.add(table);
            document.close();
            writer.close();
            System.out.println("Pdf file was saved");
            return fileName;
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return null;
    }
    private String gradeToPdf(Iterable<Grade> lst)
    {
        System.out.println("Start writing pdf");
        Document document = new Document();
        try
        {
            Random rnd = new Random();
            int nr = (rnd.nextInt(999999-9999) + 9999);
            String fileName = "Raport"+nr+".pdf";
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
            document.open();

            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100);
            table.setSpacingBefore(10f);
            table.setSpacingAfter(10f);

            float[] columnWidths = {1f, 1f, 1f,1f,1f};
            table.setWidths(columnWidths);
            Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

            PdfPCell cell1 = new PdfPCell(new Paragraph("Student Name",boldFont));
            cell1.setBorderColor(BaseColor.BLACK);
            cell1.setPaddingLeft(10);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell1.setBorderWidthTop(2);
            PdfPCell cell2 = new PdfPCell(new Paragraph("Grade",boldFont));
            cell2.setBorderColor(BaseColor.BLACK);
            cell2.setPaddingLeft(10);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell2.setBorderWidthTop(2);
            PdfPCell cell3 = new PdfPCell(new Paragraph("Teacher",boldFont));
            cell3.setBorderColor(BaseColor.BLACK);
            cell3.setPaddingLeft(10);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell3.setBorderWidthTop(2);
            PdfPCell cell4 = new PdfPCell(new Paragraph("Feedback",boldFont));
            cell4.setBorderColor(BaseColor.BLACK);
            cell4.setPaddingLeft(10);
            cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell4.setBorderWidthTop(2);
            PdfPCell cell5 = new PdfPCell(new Paragraph("Date",boldFont));
            cell5.setBorderColor(BaseColor.BLACK);
            cell5.setPaddingLeft(10);
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell5.setBorderWidthTop(2);
            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);
            table.addCell(cell5);

            lst.forEach(x->{
                Task task = taskService.getTask(new ObjectId(x.getTaskId()));
                Student student = studentService.getStudent(new ObjectId(task.getStudentId()));
                PdfPCell cell11 = new PdfPCell(new Paragraph(student.getName()+" "+student.getFirstName()));
                cell11.setBorderColor(BaseColor.BLACK);
                cell11.setPaddingLeft(10);
                cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell21 = new PdfPCell(new Paragraph(x.getGrade()+""));
                if(x.getGrade()<5)
                    cell21.setBorderColor(BaseColor.RED);
                else
                    cell21.setBorderColor(BaseColor.GREEN);
                cell21.setPaddingLeft(10);
                cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell31 = new PdfPCell(new Paragraph(x.getTeacher()));
                cell31.setBorderColor(BaseColor.BLACK);
                cell31.setPaddingLeft(10);
                cell31.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell31.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell41 = new PdfPCell(new Paragraph(x.getFeedback()));
                cell41.setBorderColor(BaseColor.BLACK);
                cell41.setPaddingLeft(10);
                cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell41.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell51 = new PdfPCell(new Paragraph(x.getDate().toString()));
                cell51.setBorderColor(BaseColor.BLACK);
                cell51.setPaddingLeft(10);
                cell51.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell51.setVerticalAlignment(Element.ALIGN_MIDDLE);

                table.addCell(cell11);
                table.addCell(cell21);
                table.addCell(cell31);
                table.addCell(cell41);
                table.addCell(cell51);

            });
            document.add(table);
            document.close();
            writer.close();
            System.out.println("Pdf file was saved");
            System.out.println("Open file...");
            return fileName;
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return null;
    }
    private void openFile(String fileName) {
        try {
            File file = new File(fileName);
            if(!Desktop.isDesktopSupported()){
                System.out.println("Desktop is not supported");
                return;
            }
            Desktop desktop = Desktop.getDesktop();
            if(file.exists())
                desktop.open(file);
            else
                System.out.println("I don`t find your file!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
