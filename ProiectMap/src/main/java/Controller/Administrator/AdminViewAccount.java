package Controller.Administrator;

import DataModel.Account;
import Services.AccountService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AdminViewAccount {
    public Label labelNameAdmin;
    public TableColumn<Account, String> collName;
    public TableColumn<Account, String> collUsername;
    public TableColumn<Account, String> collPermission;
    public TableColumn<Account, String> collEmail;
    public TextField tfPassword;
    public TextField tfUsername;
    public TextField tfEmail;
    public TextField tfName;
    public ChoiceBox<String> choiceBoxPermission;
    public TableView<Account> tableView;
    private Account account;
    private Stage prevStage;
    private Stage thisStage;
    private AccountService accountService;
    private ObservableList<Account> model = FXCollections.observableArrayList();
    private ObservableList<String> choiceBoxModel = FXCollections.observableArrayList();
    void setInfo(Account account, AccountService accountService, Stage prevStage, Stage thisStage) {
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.account = account;
        this.accountService = accountService;
        init();
        listen();
    }
    private void init()
    {
        List<String> permList=  new ArrayList <>();
        permList.add("Admin");permList.add("Teacher");
        choiceBoxModel.setAll(StreamSupport.stream(permList.spliterator(), false)
                .collect(Collectors.toList()));
        choiceBoxPermission.setItems(choiceBoxModel);

        this.labelNameAdmin.setText(account.getName());
        collName.setCellValueFactory(new PropertyValueFactory<>("name"));
        collPermission.setCellValueFactory(new PropertyValueFactory <>("permision"));
        collEmail.setCellValueFactory(new PropertyValueFactory <>("email"));
        collUsername.setCellValueFactory(new PropertyValueFactory <>("username"));
        tableView.setItems(model);
        updateTable();
    }
    private void updateTable()
    {
        model.setAll(StreamSupport.stream(accountService.getAllWithoutStud().spliterator(), false)
                .collect(Collectors.toList()));
    }
    private void listen(){
        tableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetails(newValue));
    }
    private void showDetails(Account account)
    {
        if(account!=null) {
            tfName.setText(account.getName());
            tfUsername.setText(account.getUsername());
            choiceBoxPermission.setValue(account.getPermision());
            tfEmail.setText(account.getEmail());
        }
        else
        {
            tfName.clear();
            tfUsername.clear();
            choiceBoxPermission.getSelectionModel().selectFirst();
            tfEmail.clear();
        }
        tfPassword.clear();

    }
    public void btnBackAction()
    {
        this.thisStage.close();
        this.prevStage.show();
    }
    public void btnAddAccount()
    {
        try {
            if(choiceBoxPermission.getSelectionModel().isEmpty())
            {
                showErrorMessage("Please select a permission!");return;
            }
            this.accountService.saveAccount(new ObjectId(),tfUsername.getText(),tfPassword.getText(),"null",tfName.getText(),choiceBoxPermission.getValue(),tfEmail.getText());
            showMessage("Account Saved!");
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnUpdateAccount()
    {
        try {
            if(tableView.getSelectionModel().isEmpty())
            { showErrorMessage("Please select account from table!");return;}
            Account acc = tableView.getSelectionModel().getSelectedItem();

            this.accountService.updateAccount(acc.getID(),tfUsername.getText(),tfPassword.getText(),"null",tfName.getText(),choiceBoxPermission.getValue(),tfEmail.getText());
            showMessage("Account Updated!");
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnDeleteAccount()
    {
        try {
            if(tableView.getSelectionModel().isEmpty())
            { showErrorMessage("Please select account from table!");return;}
            Account acc = tableView.getSelectionModel().getSelectedItem();

            if(acc!=null&&ObjectId.isValid(acc.getID().toString()))
            {
                accountService.deleteAccount(acc.getID());
                showMessage("Account deleted!");
            }
            else
            {
                showErrorMessage("A aparut o problema!");
            }
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }

    }
    private void showMessage(String text){
        updateTable();
        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private void showErrorMessage(String text){
        updateTable();
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }

}
