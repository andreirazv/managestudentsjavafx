package Controller.Administrator;

import DataModel.Account;
import Services.AccountService;
import Services.StudentService;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AdministratorView {

    public Label labelName;
    public Label labelRole;
    public Text labelEmail;
    private StudentService studentService;
    private AccountService accountService;
    private Account account;
    private Stage prevStage;
    private Stage thisStage;

    public void setServices(StudentService studentService, AccountService accountService, Account account, Stage prevStage, Stage thisStage)
    {
        this.studentService=studentService;
        this.accountService=accountService;
        this.account=account;
        this.prevStage=prevStage;
        this.thisStage=thisStage;
        init();
    }
    private void init(){
        if(!account.getName().equals("null")) {
            this.labelName.setText(account.getName());
            this.labelRole.setText(account.getPermision());
            this.labelEmail.setText(account.getEmail());
        }
        else
            throw new IllegalArgumentException("Exista o problema cu acest cont de admin!");
    }
    public void btnTeacherAction()
    {
        try {
            this.thisStage.close();
            FXMLLoader fxmlLoader = new
                    FXMLLoader(getClass().getResource("/View/AdminViewAccount.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Account Menu(Only admin)");
            stage.setScene(new Scene(root));
            AdminViewAccount controller = fxmlLoader.getController();
            controller.setInfo(account, accountService, thisStage, stage);
            stage.show();
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    private static void showMessage(String text){
        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private static void showErrorMessage(String text){
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }
    public void btnStudentAction()
    {
        try {
            this.thisStage.close();
            FXMLLoader fxmlLoader = new
                    FXMLLoader(getClass().getResource("/View/AdminViewStudent.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Student Menu(Only admin)");
            stage.setScene(new Scene(root));
            AdminViewStudent controller = fxmlLoader.getController();
            controller.setInfo(account, accountService,studentService ,thisStage, stage);
            stage.show();
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnLogoutAction()
    {
        this.account=null;
        this.thisStage.close();
        this.prevStage.show();
    }

}
