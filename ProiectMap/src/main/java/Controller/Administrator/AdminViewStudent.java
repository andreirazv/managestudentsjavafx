package Controller.Administrator;

import DataModel.Account;
import DataModel.Student;
import Services.AccountService;
import Services.StudentService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.bson.types.ObjectId;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AdminViewStudent {
    public Label labelNameAdmin;
    public TableColumn<Account, String> collName;
    public TableColumn<Account, String> collUsername;
    public TableColumn<Account, String> collPermision;
    public TableColumn<Account, String> collFirstName;
    public TableColumn<Account, String> collEmail;
    public TableColumn<Account, String> collGroup;
    public TextField tfPassword;
    public TextField tfUsername;
    public TextField tfName;
    public TextField tfEmail;
    public TextField tfFirstName;
    public TextField tfGroup;
    public TableView<Account> tableView;
    private Account account;
    private Stage prevStage;
    private Stage thisStage;
    private AccountService accountService;
    private StudentService studentService;
    private ObservableList<Account> model = FXCollections.observableArrayList();
    void setInfo(Account account, AccountService accountService, StudentService studentService, Stage prevStage, Stage thisStage) {
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.account = account;
        this.accountService = accountService;
        this.studentService = studentService;
        init();
        listen();
    }
    private void init()
    {
        updateTable();
        this.labelNameAdmin.setText(account.getName());
        collName.setCellValueFactory(new PropertyValueFactory<>("studentId"));
        collPermision.setCellValueFactory(new PropertyValueFactory <>("permision"));
        collUsername.setCellValueFactory(new PropertyValueFactory <>("username"));
        collGroup.setCellValueFactory(new PropertyValueFactory <>("studentId"));
        collFirstName.setCellValueFactory(new PropertyValueFactory <>("studentId"));
        collEmail.setCellValueFactory(new PropertyValueFactory <>("studentId"));
        tableView.setItems(model);
    }
    private void listen(){
        tableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetails(newValue));

        collEmail.setCellFactory(column -> {
            TableCell <Account, String> cell = new TableCell <Account, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Student st=studentService.getStudent(new ObjectId(item));
                        setText(st.getEmail()); } }};return cell; });

        collGroup.setCellFactory(column -> {
            TableCell <Account, String> cell = new TableCell <Account, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Student st=studentService.getStudent(new ObjectId(item));
                        setText(st.getGroup()+""); } }};return cell; });
        collName.setCellFactory(column -> {
            TableCell <Account, String> cell = new TableCell <Account, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Student st=studentService.getStudent(new ObjectId(item));
                        setText(st.getName()); } }};return cell; });
        collFirstName.setCellFactory(column -> {
            TableCell <Account, String> cell = new TableCell <Account, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        Student st=studentService.getStudent(new ObjectId(item));
                        setText(st.getFirstName()); } }};return cell; });
    }
    private void showDetails(Account accountRecv)
    {
        if(accountRecv!=null) {
            Student st = studentService.getStudent(new ObjectId((accountRecv.getStudentId())));
            if (st != null) {
                tfName.setText(st.getName());
                tfUsername.setText(accountRecv.getUsername());
                tfEmail.setText(st.getEmail());
                tfGroup.setText(st.getGroup() + "");
                tfFirstName.setText(st.getFirstName() + "");
            }
        }
    }
    public void btnBackAction()
    {
        this.thisStage.close();
        this.prevStage.show();
}
    public void btnAddStudentAction()
    {
        int gr;
        try {
            gr  =Integer.parseInt(tfGroup.getText());
        }
        catch (Exception ex)
        {
            showErrorMessage("Group should be a number!\n");
            return;
        }
        try {
            ObjectId id=this.studentService.saveStudent(new ObjectId(),tfName.getText(),tfFirstName.getText(),gr,tfEmail.getText(),0);
            this.accountService.saveAccount(new ObjectId(),tfUsername.getText(),tfPassword.getText(),id.toString(),"null","Student","");
            showMessage("Account Added");
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnUpdateAction()
    {
        try {
            if(tableView.getSelectionModel().isEmpty())
            { showErrorMessage("Please select account from table!");return;}
            Account acc = tableView.getSelectionModel().getSelectedItem();
            Student st = studentService.getStudent(new ObjectId((acc.getStudentId())));
            studentService.updateStudent(st.getID(),tfName.getText(),tfFirstName.getText(),Integer.parseInt(tfGroup.getText()),tfEmail.getText(),st.getExtraPoints(),st.isIdle());
            if(!tfPassword.getText().equals(""))
                this.accountService.updateAccount(acc.getID(),tfUsername.getText(),tfPassword.getText(),acc.getStudentId(),"null","Student","");
            else
                this.accountService.updateAccount(acc.getID(),tfUsername.getText(),"",acc.getStudentId(),"null","Student","");
            showMessage("Account Updated");

        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    public void btnDeleteAction()
    {
        try {
            if(tableView.getSelectionModel().isEmpty())
            { showErrorMessage("Please select account from table!");return;}
            Account acc = tableView.getSelectionModel().getSelectedItem();
            if(acc!=null&&ObjectId.isValid(acc.getID().toString()))
            {
                Student st = studentService.getStudent(new ObjectId((acc.getStudentId())));
                studentService.deleteStudent(st.getID());
                accountService.deleteAccount(acc.getID());
                showMessage("Account Deleted");
            }
            else
            {
                showErrorMessage("Something is wrong");
            }
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    private void showMessage(String text){
        updateTable();
        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private void showErrorMessage(String text){
        updateTable();
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }
    private void updateTable()
    {
        model.clear();
        model.setAll(StreamSupport.stream(accountService.getAllStud().spliterator(), false)
                .collect(Collectors.toList()));
        tableView.setItems(model);
    }
}
