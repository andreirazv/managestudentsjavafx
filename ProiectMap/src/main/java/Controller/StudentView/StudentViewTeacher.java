package Controller.StudentView;

import Controller.Observer.AutoCompleteComboBoxListener;
import DataModel.Account;
import DataModel.Student;
import Services.StudentService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class StudentViewTeacher {
    public Label labelTeacherName;
    Account account;
    public TableColumn<Student, String> collName;
    public TableColumn<Student,String> collFirstName;
    public TableColumn<Student, String> collEmail;
    public TableColumn<Student, Integer> collGroup;
    public TableColumn<Student, Double> collExtraPoints;
    public TableView<Student> tableView;
    private Stage prevStage;
    private Stage thisStage;
    private StudentService studentService;
    public ObservableList<Student> model = FXCollections.observableArrayList();

    public ComboBox<String> cboxStudentFiltr;
    public ComboBox<String> cboxGroupFiltr;
    public TextField tfExtraPoints;


    public void setStudent(Account account, StudentService studentService, Stage prevStage, Stage thisStage) {
        this.studentService = studentService;
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.account = account;
        init();

    }
    private void init()
    {

        this.labelTeacherName.setText(account.getName());
        model.setAll(StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .collect(Collectors.toList()));
        collName.setCellValueFactory(new PropertyValueFactory<>("name"));
        collFirstName.setCellValueFactory(new PropertyValueFactory <>("firstName"));
        collEmail.setCellValueFactory(new PropertyValueFactory <>("email"));
        collGroup.setCellValueFactory(new PropertyValueFactory <>("group"));
        collExtraPoints.setCellValueFactory(new PropertyValueFactory <>("extraPoints"));
        tableView.setItems(model);
        initComboBox();
    }
    private void initComboBox()
    {
        initComboBoxStudent();
        initComboBoxGroup();
    }
    private void initComboBoxStudent()
    {
        List<String> list=StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .map(st ->st.getName()+" "+st.getFirstName())
                .collect(Collectors.toList());
        List<String> listWithoutDuplicates = new ArrayList<>(
                new HashSet<>(list));
        cboxStudentFiltr.getItems().addAll(listWithoutDuplicates);

        new AutoCompleteComboBoxListener<String>(cboxStudentFiltr);
    }
    private void initComboBoxGroup()
    {
        List<String> list=StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .map(st ->st.getGroup()+"")
                .collect(Collectors.toList());
        List<String> listWD = new ArrayList<>(
                new HashSet<>(list));
        cboxGroupFiltr.getItems().addAll(listWD);

        new AutoCompleteComboBoxListener<String>(cboxGroupFiltr);
    }
    public void cboxGroupAction()
    {
        if(cboxGroupFiltr.getValue()!=null&&!cboxGroupFiltr.getValue().equals("")) {
            model.clear();
            model.setAll(StreamSupport.stream(studentService.findAllStudent(Integer.parseInt(cboxGroupFiltr.getValue()), false).spliterator(), false)
                    .collect(Collectors.toList()));
            tableView.setItems(model);
        }
    }
    public void cboxStudentAction() {
        if (cboxStudentFiltr.getValue()!=null&&!cboxStudentFiltr.getValue().equals("")) {
            model.clear();
            model.setAll(StreamSupport.stream(studentService.findAllStudent(cboxStudentFiltr.getValue(), false).spliterator(), false)
                    .collect(Collectors.toList()));
            tableView.setItems(model);
        }
    }
    public void btnAddExtraPointsAction()
    {
        try {
            Student student = tableView.getSelectionModel().getSelectedItem();
            if (student != null && !tfExtraPoints.getText().equals("")) {
                try {
                    double extraPoints = Double.parseDouble(tfExtraPoints.getText());
                    studentService.updateExtrapoints(student.getID(), extraPoints);
                    showMessage(student.getName()+" "+student.getFirstName()+" recived "+extraPoints+" extra points!");
                } catch (Exception ex) {
                    showErrorMessage(ex.getMessage());
                }
            }
            else
                showErrorMessage("Something is wrong");
        }
        catch (Exception ex)
        {
            showErrorMessage(ex.getMessage());
        }
    }
    private void updateTable()
    {
        model.clear();
        model.setAll(StreamSupport.stream(studentService.findAllStudent(false).spliterator(), false)
                .collect(Collectors.toList()));
        tableView.setItems(model);
    }
    public void btnBackAction()
    {
        thisStage.close();
        prevStage.show();
    }
    private void showMessage(String text){
        updateTable();
        Alert message=new Alert(Alert.AlertType.INFORMATION);
        message.setHeaderText("Information");
        message.setContentText(text);
        message.showAndWait();
    }
    private void showErrorMessage(String text){
        updateTable();
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Eroare!!!");
        message.setContentText(text);
        message.showAndWait();
    }
}
