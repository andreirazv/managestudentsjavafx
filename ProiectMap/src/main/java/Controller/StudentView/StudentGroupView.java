package Controller.StudentView;

import DataModel.Student;
import Services.StudentService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class StudentGroupView {
    @FXML
    private Label labelGroup;
    @FXML
    private Label labelStudent;
    private Student student;
    @FXML
    private TableColumn<Student, String> collName;
    @FXML
    private TableColumn<Student,String> collFirstName;
    @FXML
    private TableColumn<Student, String> collEmail;
    @FXML
    private TableColumn<Student, Integer> collGroup;

    @FXML
    private TableView<Student> tableView;
    private Stage prevStage;
    private Stage thisStage;
    private StudentService studentService;
    private ObservableList<Student> model = FXCollections.observableArrayList();

    public void setStudent(Student student, StudentService studentService, Stage prevStage, Stage thisStage) {
        this.studentService = studentService;
        this.thisStage = thisStage;
        this.prevStage = prevStage;
        this.student = student;
        init();

    }
    private void init()
    {
        this.labelGroup.setText(""+student.getGroup());
        this.labelStudent.setText(student.getName()+" "+student.getFirstName());
        model.setAll(StreamSupport.stream(studentService.findAllStudent(student,false).spliterator(), false)
                .collect(Collectors.toList()));
        collName.setCellValueFactory(new PropertyValueFactory<>("name"));
        collFirstName.setCellValueFactory(new PropertyValueFactory <>("firstName"));
        collEmail.setCellValueFactory(new PropertyValueFactory <>("email"));
        collGroup.setCellValueFactory(new PropertyValueFactory <>("group"));
        tableView.setItems(model);
    }
    public void btnBackAction()
    {
        thisStage.close();
        prevStage.show();
    }
}
