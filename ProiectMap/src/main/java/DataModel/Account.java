package DataModel;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.MessageFormat;

public class Account implements HasID<ObjectId> {

    private ObjectId _id;
    private String username;
    private String password;
    private String studentId;
    private String permision;
    private String name;
    private String email;
    @Override
    public String toString() {
        return
                "{" +
                    "username:"+MessageFormat.format("\"{0}\"", username) +
                    "password:"+MessageFormat.format("\"{0}\"", password) +
                    "studentId:"+MessageFormat.format("\"{0}\"", studentId) +
                    "name:"+MessageFormat.format("\"{0}\"", name) +
                    "email:"+MessageFormat.format("\"{0}\"", email) +
                    "permision:"+MessageFormat.format("\"{0}\"", permision) +
                "}";
    }

    public Account(ObjectId _id, String username, String password, String studentId,String name,String permision,String email) {
        this._id = _id;
        this.username = username;
        this.name = name;
        this.password = password;
        this.studentId = studentId;
        this.permision = permision;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Account getObj(Document doc) {
        return new Account(doc.getObjectId("_id"),doc.getString("username"),doc.getString("password"),doc.getString("studentId"),doc.getString("name"),doc.getString("permision"),doc.getString("email"));
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getPermision() {
        return permision;
    }

    public void setPermision(String permision) {
        this.permision = permision;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Override
    public ObjectId getID() {
        return _id;
    }

    @Override
    public void setID(ObjectId objectId) {
        this._id=objectId;
    }
}
