package DataModel.Validator;

import DataModel.Student;
import org.bson.types.ObjectId;

import static Utils.Utils.EMAIL_PATTERN;

public class StudentValidator implements Validator<Student> {

    @Override
    public void validate(Student entity){
        String err="";
        //validate entity
        if(!ObjectId.isValid(entity.getID().toString()))
            err+="This id is not valid!\n";
        if(!entity.getEmail().matches(EMAIL_PATTERN))
            err="Email is not valid!\nModel: %@%.%\n";

        if(entity.getGroup()<100||entity.getGroup()>999)
            err+="Student group must to be between 100 and 999\n";
        if(entity.getExtraPoints()<0)
            err+="This student can't have negative points\n";
        if(entity.getName().equals(""))
            err+="Please insert name for student\n";
        if(entity.getFirstName().equals(""))
            err+="Please insert firstname for student\n";
        if (!err.equals(""))
            throw new ValidationException(err);

    }
}
