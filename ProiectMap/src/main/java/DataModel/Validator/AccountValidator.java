package DataModel.Validator;

import DataModel.Account;
import org.bson.types.ObjectId;

import static Utils.Utils.EMAIL_PATTERN;

public class AccountValidator  implements Validator<Account> {

    @Override
    public void validate(Account entity){
        String err="";
        //validate entity
        if(!ObjectId.isValid(entity.getID().toString()))
            err+="This id is not valid!\n";
        if(entity.getStudentId().equals("null")&&(!entity.getEmail().matches(EMAIL_PATTERN)))
            err="Email is not valid!\nModel: %@%.%\n";

        if(entity.getUsername().equals(""))
            err+="Please insert username\n";
        if(entity.getStudentId().equals("null")&&entity.getName().equals(""))
            err+="Please insert name for account\n";

        if (!err.equals(""))
            throw new ValidationException(err);

    }
}
