package DataModel.Validator;

import DataModel.Grade;
import org.bson.types.ObjectId;

import static Utils.Utils.getCurrentWeek;

public class GradeValidator implements Validator<Grade> {

    @Override
    public void validate(Grade entity){
        String err="";
        if(!ObjectId.isValid(entity.getID().toString()))
            err+="This id is not valid!\n";
        if(entity.getGrade()<1||entity.getGrade()>10)
            err+="Grade should be between 1 and 10!\n";
        if(entity.getTeacher().equals(""))
            err+="You need to complete teacher`s field!\n";
        if(entity.getFeedback().equals(""))
            err+="You need to insert feedback!\n";
        if(entity.getTaskId().equals(""))
            err+="You need to insert feedback!\n";
        if(entity.getTaskId().equals(""))
            err+="Task is not valid!\n";

        try{
            getCurrentWeek(entity.getDate());
        }
        catch (Exception ex)
        {
            err+=ex.getMessage()+"\n";
        }
        //validate entity
        if (!err.equals(""))
            throw new ValidationException(err);

    }
}
