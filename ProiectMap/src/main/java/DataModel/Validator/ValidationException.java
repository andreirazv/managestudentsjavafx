package DataModel.Validator;

public class ValidationException extends RuntimeException{

    ValidationException(String err) {
        super(err);
    }
}