package DataModel.Validator;

public interface Validator<E> {
    void validate(E entity) ;
}
