package DataModel.Validator;

import DataModel.Task;
import org.bson.types.ObjectId;

import java.time.LocalDate;

import static Utils.Utils.getCurrentWeek;

public class TaskValidator implements Validator<Task> {

    @Override
    public void validate(Task entity){
        String err="";
        if(!ObjectId.isValid(entity.getID().toString()))
            err+="This id is not valid!\n";
        if(entity.getTeacher().equals(""))
            err+="Please complete teacher's name\n";
        if(entity.getDescription().equals(""))
            err+="Please insert some description!\n";
        if(entity.getStudentId().equals(""))
            err+="It is problem with student!\n";
        if(entity.getDeadLine().toString().equals(""))
            err+="Please insert a valid date!\n";
        if(entity.getDeadLine().isBefore(LocalDate.now()))
            err+="Deadline should be greater then today!\n";
        try{
            getCurrentWeek(entity.getDeadLine());
        }
        catch (Exception ex)
        {
            err+=ex.getMessage()+"\n";
        }
        if (!err.equals(""))
            throw new ValidationException(err);

    }
}
