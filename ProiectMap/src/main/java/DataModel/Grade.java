package DataModel;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.MessageFormat;
import java.time.LocalDate;

public class Grade implements HasID<ObjectId>{

    private ObjectId _id;
    private double grade;
    private String taskId;
    private String teacher;
    private String feedback;
    private LocalDate date;

    public Grade() {}
    public Grade(ObjectId _id, double grade, String taskId, String teacher,String feedback, LocalDate date) {
        this._id = _id;
        this.grade = grade;
        this.taskId = taskId;
        this.teacher = teacher;
        this.feedback = feedback;
        this.date = date;
    }
    public String toString() {
        return
                "{" +
                        " grade:" + MessageFormat.format("{0}", grade) +
                        " taskId:" + MessageFormat.format("\"{0}\"", taskId) +
                        " teacher:" + MessageFormat.format("\"{0}\"", teacher) +
                        " feedback:" + MessageFormat.format("\"{0}\"", feedback) +
                        " date:" + MessageFormat.format("\"{0}\"", date) +
                        "}"
                ;
    }
    public static Grade getObj(Document doc) {
        LocalDate date = LocalDate.parse(doc.getString("date"));
        double grade;
        try
        {
            grade=doc.getDouble("grade");
        }
        catch (Exception ex)
        {
            grade=doc.getInteger("grade");
        }
        return new Grade(doc.getObjectId("_id"),grade,doc.getString("taskId"),doc.getString("teacher"),doc.getString("feedback"),date);
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public ObjectId getID() {
        return _id;
    }

    @Override
    public void setID(ObjectId objectId) {
        this._id=objectId;
    }

}
