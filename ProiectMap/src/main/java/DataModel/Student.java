package DataModel;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.MessageFormat;

import static Utils.Utils.hashMd5;

public class Student  implements HasID<ObjectId>{

    public Student() {}
    public Student(ObjectId idStudent, String name, String firstName, int group, String email, double extraPoints, boolean idle) {
        this(idStudent,name,firstName,group,email,extraPoints);
        this.idle=idle;
    }
    public Student(ObjectId idStudent, String name, String firstName, int group, String email, double extraPoints) {
        this._id = idStudent;
        this.name = name;
        this.firstName = firstName;
        this.group = group;
        this.email = email;
        this.extraPoints=extraPoints;
        this.idle=false;
    }
    private ObjectId _id;
    private String name;
    private String firstName;
    private int group;
    private String email;
    private double extraPoints;
    private boolean idle;

    public double getExtraPoints() { return extraPoints; }
    public void setExtraPoints(double extraPoints) { this.extraPoints = extraPoints; }
    public String getName() {
        return name;
    }
    public int getGroup() {
        return group;
    }
    public String getEmail() {
        return email;
    }
    public boolean isIdle() { return idle; }
    public void setIdle(boolean idle) { this.idle = idle; }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public ObjectId getID() {
        return _id;
    }

    @Override
    public void setID(ObjectId s) {
        this._id=s;
    }

    @Override
    public String toString() {
        return
               "{" +
                   "name:"+MessageFormat.format("\"{0}\"", name) +
                   "firstName:"+MessageFormat.format("\"{0}\"", firstName) +
                   " group:"+MessageFormat.format("{0}", group) +
                   " email:"+MessageFormat.format("\"{0}\"", email) +
                   " extraPoints:"+MessageFormat.format("{0}", extraPoints) +
                   " idle:"+MessageFormat.format("{0}", idle) +
               "}"
                ;
    }
    public static Student getObj(Document doc) {
        double extraPoints;
        try {
            extraPoints = doc.getDouble("extraPoints");
        }
        catch (Exception ex)
        {
            extraPoints = doc.getInteger("extraPoints");
        }
        return new Student(doc.getObjectId("_id"),doc.getString("name"),doc.getString("firstName"),doc.getInteger("group"),doc.getString("email"),extraPoints,doc.getBoolean("idle",false));
    }
}