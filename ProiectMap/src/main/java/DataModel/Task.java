package DataModel;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.MessageFormat;
import java.time.LocalDate;

public class Task implements HasID<ObjectId>{

    private ObjectId _id;
    private String description;
    private LocalDate deadLine;
    private String studentId;
    private String teacher;

    public Task() {}
    public Task(ObjectId taskId, String description, LocalDate deadLine, String studentId, String teacher) {
        this._id = taskId;
        this.description = description;
        this.deadLine = deadLine;
        this.studentId = studentId;
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return
                "{" +
                        " description:" + MessageFormat.format("\"{0}\"", description) +
                        " deadline:" + MessageFormat.format("\"{0}\"", deadLine) +
                        " studentId:" + MessageFormat.format("\"{0}\"", studentId)  +
                        " teacher:" + MessageFormat.format("\"{0}\"", teacher) +
                        "}"
                ;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(LocalDate deadLine) {
        this.deadLine = deadLine;
    }

    public String  getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    @Override
    public ObjectId getID() {
        return _id;
    }

    @Override
    public void setID(ObjectId objectId) {
        this._id=objectId;
    }

    public static Task getObj(Document doc) {
        LocalDate date = LocalDate.parse(doc.getString("deadline"));
        return new Task(doc.getObjectId("_id"),doc.getString("description"),date,doc.getString("studentId"),doc.getString("teacher"));

    }
}
