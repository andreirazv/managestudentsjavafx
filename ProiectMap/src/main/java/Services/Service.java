package Services;

import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import Repository.CrudRepository;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Service{

    CrudRepository<ObjectId, Student> repoStud;
    CrudRepository<ObjectId, Task> repoTask ;
    CrudRepository<ObjectId, Grade> repoGrade;


    Service(CrudRepository <ObjectId, Student> repoStud,
            CrudRepository <ObjectId, Task> repoTask,
            CrudRepository <ObjectId, Grade> repoGrade) {
        this.repoStud = repoStud;
        this.repoTask = repoTask;
        this.repoGrade = repoGrade;
    }

    public Student getStudent(ObjectId id)
    {
        try {
            return repoStud.findOne(id);
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    <T> Iterable <T> filter(Iterable <T> list, Predicate <T> cond)
    {
        List<T> rez=new ArrayList<>();
        list.forEach((T x)->{if (cond.test(x)) rez.add(x);});
        return rez;
    }

    public Task getTask(ObjectId idTask) {
        return repoTask.findOne(idTask);
    }

    public Grade getGrade(ObjectId idGrade)
    {
        return repoGrade.findOne(idGrade);
    }
}
