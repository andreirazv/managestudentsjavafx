package Services;

import Controller.Observer.GradeChangeEvent;
import Controller.Observer.Observable;
import Controller.Observer.Observer;
import DataModel.Account;
import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import Repository.CrudRepository;
import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static Utils.Utils.*;


public class GradeService extends Service implements Observable<GradeChangeEvent> {

    public GradeService(CrudRepository<ObjectId, Student> repoStud,
                       CrudRepository<ObjectId, Task> repoTask,
                       CrudRepository<ObjectId, Grade> repoNota) {
        super(repoStud,repoTask,repoNota);
    }
    public void saveGrade(ObjectId idGrade, double grade, String teacherNota, LocalDate date, ObjectId idTask, String feedback, boolean motivat, Account account) {

        List<Grade> lst = new ArrayList<>();
        filter(findAllGrades(),(Grade n)-> n.getTaskId().equals(idTask.toString())).forEach(elem->lst.add(elem));
        if(lst.size()!=0)
            throw new IllegalArgumentException("A fost data deja o nota pentru aceasta tema");
        Task hw= getTask(idTask);
        Grade nt = new Grade(idGrade,grade,hw.getID().toString(),teacherNota,feedback,date);
        idGrade=repoGrade.save(nt);
        LocalDate deadLineWeek=hw.getDeadLine();
        Student st = getStudent(new ObjectId(hw.getStudentId()));
        double extraPoints=st.getExtraPoints();
            int week = getCurrentWeek(currentDate)-getCurrentWeek(deadLineWeek);
            if (week > 2&&!motivat)
                grade = 1;
            else {
                if (week <= 0)
                    week = 0;
                if(!motivat)
                    grade = grade - (2.5 * week);

                grade = grade + extraPoints;
                if (grade <= 10)
                    extraPoints = 0;
                else {
                    extraPoints = grade - 10;
                    grade = 10;
                }
            }

        Student st1 = new Student(st.getID(),st.getName(),st.getFirstName(),st.getGroup(),st.getEmail(),extraPoints,st.isIdle());
        repoStud.update( st1);

        Task hw1 = new Task(hw.getID(),hw.getDescription(),hw.getDeadLine(),st1.getID().toString(),hw.getTeacher());
        Grade nt2=new Grade(idGrade,grade,hw1.getID().toString(),teacherNota,feedback,date);
        final double messageGrade = grade;
        final double extrPctMessage = extraPoints;
        repoGrade.update(nt2);
        new Thread(() -> sendSimpleTestEmail(st.getEmail(),account.getEmail(),"New Grade",
                "You recived "+messageGrade+" mark"+
                        "\n"+"Task date:"+date+
                        "\n"+"You have "+extrPctMessage+" extra points remiaing"+
                        "\n"+"Feedback "+feedback+
                        "\n\n"+"Teacher:"+teacherNota)).start();

    }

    public void updateGrade(ObjectId idGrade, double grade, String teacherNota, LocalDate date,ObjectId idTask,String feedback) {
        Task task=getTask(idTask);
        Grade nt = new Grade(idGrade,grade,task.getID().toString(),teacherNota,feedback,date);
        repoGrade.update(nt);
    }
    public Iterable<Grade> findAllGrades() {
        return repoGrade.findAll();
    }
    public Iterable<Grade> findAllGrades(LocalDate date) {
        return filter(findAllGrades(),(Grade n)-> n.getDate().equals(date));
    }
    public Iterable<Grade> findAllGradesByDate(int nrDate) {
        return filter(findAllGrades(),(Grade n)->getCurrentWeek(getTask(new ObjectId(n.getTaskId())).getDeadLine())==nrDate);
    }
    public Iterable<Grade> findAllGradesByDate(int nrDate,String group) {
        return filter(findAllGrades(),(Grade n)->getCurrentWeek(getTask(new ObjectId(n.getTaskId())).getDeadLine())==nrDate&& (getStudent(new ObjectId(getTask(new ObjectId(n.getTaskId())).getStudentId())).getGroup()+"").equals(group));
    }
    public Iterable<Grade> findAllGrades(LocalDate from,LocalDate to) {
        return filter(findAllGrades(),(Grade n)-> n.getDate().isAfter(from)&&n.getDate().isBefore(to));
    }
    public Iterable<Grade> findAllGrades(Student student) {
        return filter(findAllGrades(),(Grade n)-> getTask(new ObjectId(n.getTaskId())).getStudentId().equals(student.getID().toString()));
    }
    public Iterable<Grade> findAllGrades(int group) {
        return filter(findAllGrades(),(Grade n)-> (getStudent(new ObjectId(getTask(new ObjectId(n.getTaskId())).getStudentId())).getGroup()+"").contains(group+""));
    }
    public Iterable<Grade> findAllGrades(String name) {
        return filter(findAllGrades(),(Grade n)-> (getStudent(new ObjectId(getTask(new ObjectId(n.getTaskId())).getStudentId())).getName()+" "+getStudent(new ObjectId(getTask(new ObjectId(n.getTaskId())).getStudentId())).getFirstName()).contains(name));
    }
    public void deleteGrade(ObjectId id) {
        repoGrade.delete(id);
    }
    private List<Observer<GradeChangeEvent>> observers=new ArrayList<>();

    @Override
    public void addObserver(Observer<GradeChangeEvent> e) {
        observers.add(e);

    }

    @Override
    public void removeObserver(Observer<GradeChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(GradeChangeEvent t) {

        observers.forEach(x->x.update(t));
    }
}
