package Services;

import Controller.Observer.TaskChangeEvent;
import Controller.Observer.Observable;
import Controller.Observer.Observer;
import DataModel.Account;
import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import Repository.CrudRepository;
import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static Utils.Utils.currentDate;
import static Utils.Utils.sendSimpleTestEmail;

public class TaskService extends Service implements Observable<TaskChangeEvent> {
    public TaskService(CrudRepository<ObjectId, Student> repoStud,
                           CrudRepository<ObjectId, Task> repoTask,
                           CrudRepository<ObjectId, Grade> repoNota) {
       super(repoStud,repoTask,repoNota);
    }


    public void saveTask(ObjectId idTask, String description, LocalDate deadLineWeek, ObjectId idStudent, String teacher, Account account) {

        Student st=getStudent(idStudent);

        findAllFinishedHw().forEach(hws->{if(hws.getID().equals(idTask))
            throw new IllegalArgumentException("(Save) - Acest Id:"+ hws.getID()+" mai exista in teme terminate!" + "\n");
            });

        Task hw = new Task(idTask, description, deadLineWeek, st.getID().toString(), teacher);

        repoTask.save( hw);
        new Thread(() -> sendSimpleTestEmail(st.getEmail(),account.getEmail(),"You have new Task!",
                        "Student Name:"+st.getName()+" "+st.getFirstName()+"\n"+
                        "Student Group:"+st.getGroup()+"\n"+
                        "Student Extra points:"+st.getExtraPoints()+"\n"+
                        "Description:"+description+"\n"+
                        "Deadline:"+deadLineWeek+"\n"+
                        "\n"+ "Have a great day,"+
                        "\n"+"Teacher:"+teacher)).start();
    }

    public void updateTaskDeadLine(ObjectId idTask,int weeks,String teacher,Account account) {
        Task findHw = getTask(idTask);
        Student st = getStudent(new ObjectId(findHw.getStudentId()));
        LocalDate deadLine = findHw.getDeadLine();
        if(deadLine.compareTo(currentDate)<0)
            throw new IllegalArgumentException("Data curenta este mai mare decat data pentru deadline\n");
        final LocalDate deadLineBefore = deadLine;
        deadLine=deadLine.plus(weeks, ChronoUnit.WEEKS);
        findHw.setDeadLine(deadLine);
        final LocalDate deadLineAfter = deadLine;
        if(!repoTask.update(findHw))
            throw new IllegalArgumentException("(Update) - Acest Id nu exista in baza de date!" + "\n");
        new Thread(() -> sendSimpleTestEmail(st.getEmail(),account.getEmail(),"Deadline updated!",
                "Your deadline was updated!"+"\n"+
                                "Old Deadline:"+deadLineBefore+"\n"+
                                "New Deadline:" +deadLineAfter+"\n"+
                                "\n"+"Teacher:"+teacher)).start();
    }
    public Iterable<Task> findAllNotFinishedHw(Student student) {
        return filter(findAllNotFinishedHw(),(Task n)-> getStudent(new ObjectId(n.getStudentId())).getID().toString().equals(student.getID().toString()));
    }
    public Iterable<Task> findAllNotFinishedHw(LocalDate date) {
        return filter(findAllNotFinishedHw(),(Task n)->n.getDeadLine().isEqual(date));
    }
    public Iterable<Task> findAllNotFinishedHw(LocalDate from,LocalDate to) {
        return filter(findAllNotFinishedHw(),(Task n)->n.getDeadLine().isAfter(from)&&n.getDeadLine().isBefore(to));
    }
    public Iterable<Task> findAllNotFinishedHw(int group) {
        return filter(findAllNotFinishedHw(),(Task n)-> (getStudent(new ObjectId(n.getStudentId())).getGroup()+"").contains(group+""));
    }
    public Iterable<Task> findAllNotFinishedHw(String name) {
        return filter(findAllNotFinishedHw(),(Task n)-> (getStudent(new ObjectId(n.getStudentId())).getName()+" "+getStudent(new ObjectId(n.getStudentId())).getFirstName()).contains(name+""));
    }
    public Iterable<Task> findAllFinishedHw(Student student) {
        return filter(findAllFinishedHw(),(Task n)-> getStudent(new ObjectId(n.getStudentId())).getID().toString().equals(student.getID().toString()));
    }
    public Iterable<Task> findAllNotFinishedHw() {
        return repoTask.findAll();
    }
    private Iterable<Task> findAllFinishedHw() {
        List<Task> lst = new ArrayList<>();
        repoGrade.findAll().forEach((Grade e)->lst.add(getTask(new ObjectId(e.getTaskId()))));
        return lst;
    }
    public void deleteTask(ObjectId id) {
        if(!repoTask.delete(id))
            throw new IllegalArgumentException("(Delete) - Acest Id nu exista in baza de date!" + "\n");
    }
    private List<Observer<TaskChangeEvent>> observers=new ArrayList<>();

    @Override
    public void addObserver(Observer<TaskChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<TaskChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(TaskChangeEvent t) {

        observers.forEach(x->x.update(t));
    }
}
