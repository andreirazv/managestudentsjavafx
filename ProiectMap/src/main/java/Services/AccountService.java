package Services;

import DataModel.*;
import Repository.CrudRepository;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.StreamSupport;

import static Utils.Utils.hashMd5;

public class AccountService {
    private CrudRepository<ObjectId, Account> repoAccount;
    public AccountService(CrudRepository<ObjectId, Account> repoAccount) {
        this.repoAccount=repoAccount;
    }
    public void saveAccount(ObjectId id,String username,String password,String studentId,String name,String permision,String email)
    {
        if(existAccount(username))
            throw new IllegalArgumentException("You should choose another username!");
        if(password.length()<3)
            throw new IllegalArgumentException("Password should have at least 3 characters\n");
        Account account = new Account(id,username,hashMd5(password),studentId,name,permision,email);
        this.repoAccount.save(account);
    }
    public void updateAccount(ObjectId id,String username,String password,String studentId,String name,String permision,String email)
    {
        Account account = repoAccount.findOne(id);
        account.setName(name);
        if(!password.equals(""))
            account.setPassword(hashMd5(password));
        account.setPermision(permision);
        account.setEmail(email);
        account.setUsername(username);
        this.repoAccount.update(account);
    }
    public void deleteAccount(ObjectId id)
    {
        this.repoAccount.delete(id);
    }
    public Iterable<Account> getAllWithoutStud()
    {
        return filter(repoAccount.findAll(), account ->account.getStudentId().equals("null")&&!account.getName().equals("null"));
    }
    public Iterable<Account> getAllStud()
    {
        return filter(repoAccount.findAll(), account ->!account.getStudentId().equals("null")&&account.getName().equals("null"));
    }
    private boolean existAccount(String username)
    {
        return filter(repoAccount.findAll(), account ->account.getUsername().equals(username)).iterator().hasNext();
    }

    private <T> Iterable <T> filter(Iterable <T> list, Predicate <T> cond)
    {
        List<T> rez=new ArrayList<>();
        list.forEach((T x)->{if (cond.test(x)) rez.add(x);});
        return rez;
    }
    public Account getStudentByLogin(String username, String password) {
        String pass = hashMd5(password);
        Iterable<Account> accountList = filter(repoAccount.findAll(), account ->account.getUsername().equals(username)&&account.getPassword().equals(pass));
        if(StreamSupport.stream(accountList.spliterator(), false).count()==1)
            return StreamSupport.stream(accountList.spliterator(), false).findFirst().get();
        throw new IllegalArgumentException("Username or password is wrong!");
    }


}
