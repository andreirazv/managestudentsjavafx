package Services;

import Controller.Observer.Observable;
import Controller.Observer.Observer;
import Controller.Observer.StudentChangeEvent;
import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import Repository.CrudRepository;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

import static Utils.Utils.hashMd5;

public class StudentService extends Service implements Observable<StudentChangeEvent> {

    public StudentService(CrudRepository<ObjectId, Student> repoStud,
                          CrudRepository<ObjectId, Task> repoTask,
                          CrudRepository<ObjectId, Grade> repoNota) {
        super(repoStud,repoTask,repoNota);
    }

    public ObjectId saveStudent(ObjectId idStudent, String name,String firstName, int group, String email, double extraPoints) {
        Student student = getStudent(idStudent);
        if(student!=null)
            throw new IllegalArgumentException("Somethings is going wrong!");
        Student st = new Student(idStudent,name,firstName,group,email,extraPoints,false);
        return repoStud.save(st);
    }
    public ObjectId saveStudent(Student student) {
        return repoStud.save(student);
    }


    public void updateStudent(ObjectId idStudent, String name,String firstName, int group,String email,double extraPoints,boolean idle) {
            Student student = this.getStudent(idStudent);
            student.setName(name);
            student.setFirstName(firstName);
            student.setGroup(group);
            student.setExtraPoints(extraPoints);
            student.setEmail(email);
            student.setIdle(idle);
            repoStud.update(student);
    }
    public void updateExtrapoints(ObjectId idStudent,double extraPoints) {
            Student student = this.getStudent(idStudent);
            student.setExtraPoints(extraPoints);
            repoStud.update(student);
    }
    public void updateStudent(Student student) {
            repoStud.update(student);
    }


    public int getGroupByName(String name) {
        return filter(repoStud.findAll(), student -> (student.getName()+" "+student.getFirstName()).equals(name)).iterator().next().getGroup();
    }
    public Iterable<Student> findAllStudent(boolean isIdle) {
        return filter(repoStud.findAll(), student -> student.isIdle()==isIdle);
    }
    public Iterable<Student> findAllStudent(Student stud,boolean isIdle) {
        return filter(repoStud.findAll(), student -> student.isIdle()==isIdle&&student.getGroup()==stud.getGroup());
    }
    public Iterable<Student> findAllStudent(int group,boolean isIdle) {
        return filter(repoStud.findAll(), student -> student.isIdle()==isIdle&&(student.getGroup()+"").contains(group+""));
    }
    public Iterable<Student> findAllStudent(String name,boolean isIdle) {
        return filter(repoStud.findAll(), student -> student.isIdle()==isIdle&&(student.getName()+" "+student.getFirstName()).contains(name));
    }
    public Student getStudentByName(String fullName) {
        return filter(repoStud.findAll(), student -> (student.getName()+" "+student.getFirstName()).equals(fullName)).iterator().next();
    }
    public void deleteStudent(ObjectId id)
    {
        repoStud.delete(id);
    }
    public void idleStud(ObjectId id) {
        Student st = getStudent(id);
        boolean idleBol = false;
        if (!st.isIdle())
            idleBol = true;
        updateStudent(st.getID(),st.getName(),st.getFirstName(),st.getGroup(),st.getEmail(),st.getExtraPoints(),idleBol);
    }
    private List<Observer<StudentChangeEvent>> observers=new ArrayList<>();

    @Override
    public void addObserver(Observer<StudentChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<StudentChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(StudentChangeEvent t) {
        observers.forEach(x->x.update(t));
    }

}
