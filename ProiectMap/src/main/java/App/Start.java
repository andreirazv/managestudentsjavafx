package App;

import Controller.Login.LoginController;
import DataModel.Account;
import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import DataModel.Validator.*;
import Repository.*;
import Services.AccountService;
import Services.GradeService;
import Services.StudentService;
import Services.TaskService;
import com.mongodb.*;
import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.bson.types.ObjectId;

public class Start extends Application {
    private MongoClient mongoClient = new MongoClient();

    private Validator<Student> valStud = new StudentValidator();
    private Validator<Grade> valGrade = new GradeValidator();
    private Validator<Task> valTask = new TaskValidator();
    private Validator<Account> valAccount = new AccountValidator();
    private CrudRepository <ObjectId, Student> repStud = new StudentDatabase(mongoClient, "Student",valStud);
    private CrudRepository <ObjectId, Task> repTask = new TaskDatabase(mongoClient, "Task",valTask);
    private CrudRepository <ObjectId, Grade> repGrade = new GradeDatabase(mongoClient, "Catalog",valGrade);
    private CrudRepository <ObjectId, Account> repAccount = new AccountDatabase(mongoClient, "Account",valAccount);

    private StudentService servStud = new StudentService(repStud, repTask, repGrade);
    private TaskService servTask = new TaskService(repStud, repTask, repGrade);
    private GradeService servGrade = new GradeService(repStud, repTask, repGrade);
    private AccountService servAccount = new AccountService(repAccount);

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception{

        servAccount.saveAccount(new ObjectId(),"admin","admin","null","Nume de admin","Admin","admin@admin.com");
       FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/LoginForm.fxml"));
        Pane rootLayout = loader.load();
        LoginController controller = loader.getController();
        primaryStage.initStyle(StageStyle.UNDECORATED);
        Scene loginScene = new Scene(rootLayout);
        primaryStage.setScene(loginScene) ;
        controller.setServices(servStud,servTask,servGrade,servAccount,primaryStage);
        primaryStage.setTitle("Login");
        primaryStage.show();
    }

}
