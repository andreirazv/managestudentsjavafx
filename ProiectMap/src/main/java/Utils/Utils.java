package Utils;

import DataModel.Account;
import DataModel.Grade;
import DataModel.Student;
import DataModel.Task;
import org.bson.Document;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;
import java.util.Properties;


public class Utils {
    public static LocalDate currentDate = LocalDate.now();
    private static LocalDate startDate = LocalDate.of(2018,10,1);
    private static LocalDate startRestDate = LocalDate.of(2018,12,22);
    private static LocalDate endRestDate = LocalDate.of(2019,1,6);
    private static LocalDate endDate = LocalDate.of(2019,1,19);
    private static LocalDate endYear= LocalDate.of(2018,12,31);
    private static LocalDate startNewYear= LocalDate.of(2019,1,1);
    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static void sendSimpleTestEmail(String to,String from,String headerSubject,String contentMessage) {
        String host = "localhost";
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(headerSubject);
            message.setText(contentMessage);
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

    public static String hashMd5(String toHash) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(StandardCharsets.UTF_8.encode(toHash));
            return String.format("%032x", new BigInteger(1, md5.digest()));
        } catch ( NoSuchAlgorithmException e) {
            return null;
        }
    }
    public static int getNumbersWeek(LocalDate from,LocalDate to)
    {
           return getWeek(from)-getWeek(to);
    }

    public static int getCurrentWeek(LocalDate date)
    {
        if(date.isBefore(startDate)||date.isAfter(endDate))
            throw new IllegalArgumentException("This date is not in the current academic year");
        if((date.isAfter(startRestDate)&&date.isBefore(endYear))||(date.isAfter(startNewYear)&&date.isBefore(endRestDate)))
            throw new IllegalArgumentException("This date seems to be during the holiday!");
        if(date.isAfter(startRestDate)&&date.isBefore(endYear))
            return 12;
        if(date.isAfter(endRestDate))
            return 12+(getWeek(date)-getWeek(endRestDate))+1;
        if(date.isBefore(startRestDate))
            return getWeek(date)-getWeek(startDate)+1;
        throw new IllegalArgumentException("Wrong date");

    }
    private static int getWeek(LocalDate date)
    {
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        return date.get(weekFields.weekOfWeekBasedYear());
    }

}
